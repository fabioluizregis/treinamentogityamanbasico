# Treinamento - GIT Básico - Yaman

Treinamento Yaman - Comandos básicos de GIT

## Instalação

Para instalar o git, só entrar no link abaixo, baixar o GIT para sua versão de Sistema Operacional e instalar no melhor jeito NEXT, NEXT, ... , FINISH.

```bash
https://git-scm.com/downloads
```

## Pós-Instalação

Após instalar, abra o gerenciador de comandos em linha de sua preferência (bash, terminal, command.com) e configure seu nome de usuário e email para as credenciais GIT da sua máquina com os comandos abaixo:

```bash
git config — global user.name “Seu Nome”
git config — global user.email exemplo@seuemail.com.br
```


## Vamos ao uso!
Comandos básicos que utilizaremos neste treinamento:

#### - git clone <endereço do projeto>
Utilizamos os git clone para "clonar"o projeto para a nossa máquina.

Exemplo: Para clonar este projeto em específico, utilizamos:
```bash
git clone https://gitlab.com/fabioluizregis/treinamentogityamanbasico.git
```
### - git checkout -b <nova_branch>
Ao clonar seu projeto, normalmente ele não está na branch que você quer/precisa trabalhar.

Não é boa prática trabalhar na branch master, pois esta deve ter o projeto com o commit funcional mais atualizado.

O ideal é que você crie uma branch sua para trabalhar, normalmente com o nome da feature que irá desenvolver, assim evita estragar o que está bom caso cometa algum erro.

Exemplo de uso:
```bash
git checkout -b minha_branch
```
A sua nova branch "minha_branch" será criada, clonando a branch que se estava anteriormente, automaticamente e você será direcionado para sua nova_branch para realizar seu trabalho.

#### - git status
Utilizado para verificar as alterações realizadas no seu projeto.

#### - git add
Utilizado para adicionar as alterações que realizamos ao versionador e deixarmos essas alterações prontas para que o upload seja realizado no git.

Exemplos mais comuns de uso:
```bash
git add <nome_do_arquivo>    --> Neste caso, para adicionar um arquivo específico.
git add .     --> Neste caso, para adicionar todos os arquivos alterados.
```

#### - git commit -m "Mensagem"
Utilizado para inserir um comentário breve do que foi criado/atualizado no projeto, antes de subir para o git.

Exemplo de uso:
```bash
git commit -m "Adicionando validação de CPF ao cadastro"
```

#### - git push origin nome_da_branch_trabalhada
O push vai "empurrar" as alterações para o github, deve ser passado o nome da branch remota e a branch local que será realizado o push.

Exemplo:

Vamos supor que você clonou o projeto da Master, mudou para a branch develop e está trabalhando na sua branch, de nome minha_branch.

Sua estrutura estaria da seguinte forma:

|| master

|| ------ develop

|| -------------- minha_branch

Ao realizar o push com o comando:
```bash
git push origin minha_branch

ou

git push develop minha_branch
```

Você está subindo os arquivos alterados/criados da "minha_branch" para a branch "develop".

#### - git merge <branch>
O merge é utilizado para "mesclar" as alterações feitas em outra branch.

Por exemplo:
Fizemos alterações em minha_branch, realizamos o push da minha_branch para develop, está tudo OK, mas não aparece nada na branch master.

No caso, as alterações estão em develop e precisamos mesclar essas alterações com a a branch master para que tudo fique atualizado.

Para isto, precisamos estar na branch "desatualizada", no caso a "master" e realizar o comando abaixo, para atualizar a master com o conteúdo de develop:

```bash
git merge develop
```
Agora sua branch master estará atualizada com as alterações funcionais mais recentes.

### git branch -D <nome_da_branch>
Após finalizar um desenvolvimento, realizar o merge entre as branchs para que a branch principal esteja atualizada, é interessante que a branch que você trabalhou seja deletada para que possamos manter a organização do projeto.

No caso, você pode utilizar o comando abaixo como exemplo:
```bash
git branch -D minha_branch
```
Nota: O comando irá funcionar apenas à partir da branch de onde sua branch foi criada.

No caso, como criamos a minha_branch à partir da branch develop, o comando funcionará dentro da branch develop.

#### - git pull
Assim como o git push que empurra as alterações para o git, o git pull, puxa!

Isso mesmo, o git pull é utilizado para atualizar o projeto e/ou a branch que se quer trabalhar na sua máquina.

Para utilizar, basta estar na branch de onde todo seu trabalho será realizado, como a develop por exemplo, realizar o git pull e após isso pode criar a sua nova_branch à partir da develop e iniciar seu trabalho.

Exemplo de uso:
```bash
git pull
```

Por enquanto, é isso.

Obrigado!!